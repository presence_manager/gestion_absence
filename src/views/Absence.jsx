import React from 'react';
import styled from 'styled-components';
import {Table, Container,Row,Col, Button} from 'react-bootstrap';
import create from '../assets/img/create.png';
import axios from 'axios';
import AuthNav from '../components/Navbars/AuthNav' 
const Head = styled.p`
color: rgb(143, 201, 255);
font-size:16px;
text-align:center;
font-family: 'Cormorant Garamond', serif;`;
const Tex = styled.p`
color:black;
font-size:28px;
font-family: 'Cormorant Garamond', serif;`;


const Buttonl = styled.button`
width: 204px;
height: 49px;
background: linear-gradient(90deg, #4D9FFF 0%, rgba(255, 255, 255, 0) 70.12%), #6DD3FF;
border-radius: 50px;
border-style: none;
font-family: 'Cinzel', serif;
font-style: normal;
font-weight: normal;
font-size: 22px;
line-height: 27px;
margin-left:20px;
margin-bottom:18px;
margin-top:18px;
box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
color: #FFFFFF;
transition: all 500ms;
&:hover{
    transform: translateY(-7px);
    transition: all 500ms;
}
`;
const Lineg =styled.div`
background-color:#9cff5a;
width:20%;
norder-radius:3px
margin-left:90px;
display:inline-block;
margin-bottom:4px;
margin-right:0px !important;
height:2px;`
const Liney =styled.div`
background-color:#ffc933;
width:20%;
norder-radius:3px
display:inline-block;
margin-bottom:4px;
margin-right:0px !important;
height:2px;`
const Liner =styled.div`
background-color:#ff3370;
width:20%;
norder-radius:3px
display:inline-block;
margin-bottom:4px;
margin-right:0px !important;
height:2px;`


class Absecne extends React.Component{
    constructor(props){
        super(props);
        this.state={
            _students:[],
                _secance:[],
        }
    }
    componentWillMount(){
        axios.get('http://127.0.0.1:8090/api/students/all')
              .then(response => {console.log(response);this.setState({
                _students:response.data,
            })});

    }
    absent (data){
            
        this.setState({
            _secance:[...this.state._secance,{id:data}]
        })
        console.log(this.state._secance);
        
    }
    createTasks = (item,index) => {
        return(
            <tr key="index">
            <td>{index+1}</td>
            <td>{item.fname}</td>
            <td>{item.lname}</td>
            <td>{item.abs+" absence"} {item.abs===0?<Lineg><hr/></Lineg>:item.abs<10?<Liney><hr/></Liney>:<Liner><hr/></Liner>}</td>
            <td>{item.cne}</td>
            <td><input type="checkbox" className="chek" onClick={e=>{this.absent(item.id)}} ></input></td>
            </tr>
        )
    }
    render(){
        const listItems = this.state._students.map(this.createTasks);
        return(
            <React.Fragment>
                <AuthNav/>
            <br/><br/><br/>
                <h4 className="title">The Absence List</h4>
            <div className="decorateTable">
        <Container>
            <Row>
                <Col md="6">
                <Tex> List of Students  <i className="fa fa-arrow-right" aria-hidden="false"></i>  </Tex> 
               
                </Col>
                </Row>
        <Table striped bordered hover variant="dark" className="tb">
        <thead>
            <tr>
            <th>#</th>
            <th> <Head>First Name</Head></th>
            <th><Head>Last Name</Head></th>
            <th><Head>Total of Absence</Head></th>
            <th><Head>CNE</Head></th>
            <th><Head>Absent</Head></th>
            </tr>
        </thead>
        <tbody>
            {listItems}
        </tbody>
        <Buttonl>Confirm</Buttonl>
        </Table>
        </Container>
        </div>
        </React.Fragment>
        )
    };
}


export default  Absecne;


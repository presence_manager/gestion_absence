import React from 'react';
import styled from 'styled-components';
import {Table, Container,Row,Col} from 'react-bootstrap';
import abdo from '../../assets/img/abdo.jpg' 
import mahmoud from '../../assets/img/mahmoud.jpeg'
import aymen from '../../assets/img/aymen.jpeg'

const Tex = styled.p` 
padding-top:20px;
text-aligne:center;
padding-left:4px;
color:#ffffff;
font-family: 'Cormorant Garamond', serif;
font-size:30px;`
const Texe = styled.p` 
padding-top:20px;
text-aligne:center;
color:#ffffff;
font-family: 'Cormorant Garamond', serif;
font-size:20px;`
class Footer extends React.Component{
    render(){
        return(
            <React.Fragment>
               <footer class="custom-footer page-footer font-small  mt-lg-2 lighten-3 pt-4">
                    <div class="container">

                        <Tex>Created By:</Tex>

                    <div class="row pl-lg-5">
                        <div class="col-lg-4 col-md-12 mb-4">

                        <div class="view overlay z-depth-1-half">
                            <img src={abdo} class="img-fluid ava"
                            alt=""/>
                            <a href="">
                            <div class="mask rgba-white-light"></div>
                            </a>
                        </div>
                                <Texe>Abdellatif Ahammad</Texe>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-4">

                        
                        <div class="view overlay z-depth-1-half">
                            <img src={mahmoud} class="img-fluid ava"
                            alt=""/>
                            <a href="">
                            <div class="mask rgba-white-light"></div>
                            </a>
                        </div>
                        <Texe>Mahmoud Zakaria</Texe>
                        </div>
                        <div class="col-lg-4 col-md-6 mb-4">

                        
                            <div class="view overlay z-depth-1-half">
                                <img src={aymen} class="img-fluid ava"
                                alt=""/>
                                <a href="">
                                <div class="mask rgba-white-light"></div>
                                </a>
                            </div>
                            <Texe>El-khazenti Aymane</Texe>
                            </div>
                    

                    </div>

                    </div>

                    <div class="footer-copyright text-center py-3">© 2020 Est-SB:
                    </div>


</footer>
            </React.Fragment>
        )
    }
}

export default Footer;